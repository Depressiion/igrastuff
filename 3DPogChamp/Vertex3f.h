#pragma once
#include <string>

class Vector3f {
public:
	Vector3f();
	Vector3f(float x, float y);
	Vector3f(float x, float y, float z);
	virtual ~Vector3f();

	float x, y, z;

//operators
public:

	//+ op
	void operator+=(Vector3f input) {
		x += input.x;
		y += input.y;
		z += input.z;
	}

	//- op
	void operator-=(Vector3f input) {
		x -= input.x;
		y -= input.y;
		z -= input.z;
	}

	//* op
	void operator*=(float input) {
		x *= input;
		y *= input;
		z *= input;
	}

	// / op
	void operator /=(float input) {
		x /= input;
		y /= input;
		z /= input;
	}

//methods
public:
	inline std::string toString() {
		return  "x: " + std::to_string(x) + "\ny: " + std::to_string(y) + "\nz: " + std::to_string(z);
	}

	static Vector3f getCross(Vector3f vec1, Vector3f vec2);
	float getMagnitude();
	Vector3f getNormalised();
};

// +
inline
Vector3f operator+(Vector3f lhs, Vector3f rhs) {
	return Vector3f(lhs.x + rhs.x, lhs.y + rhs.y, lhs.z + rhs.z);
}

// -
Vector3f operator-(Vector3f lhs, Vector3f rhs) {
	return Vector3f(lhs.x - rhs.x, lhs.y - rhs.y, lhs.z - rhs.z);
}

// *
Vector3f operator*(float lhs, Vector3f rhs) {
	return Vector3f(rhs.x * lhs, rhs.y * lhs, rhs.z * lhs);
}

Vector3f operator*(Vector3f lhs, float rhs) {
	return Vector3f(lhs.x * rhs, lhs.y * rhs, lhs.z * rhs);
}

// /
Vector3f operator/(float lhs, Vector3f rhs) {
	return Vector3f(rhs.x / lhs, rhs.y / lhs, rhs.z / lhs);
}

Vector3f operator/(Vector3f lhs, float rhs) {
	return Vector3f(lhs.x / rhs, lhs.y / rhs, lhs.z / rhs);
}


