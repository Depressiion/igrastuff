#include "stdafx.h"
using namespace MyPolygon;

MyPolygon::Polygon::Polygon() {
}

Polygon::Polygon(Vector3f normal) : normal(normal) {
}

Polygon::~Polygon() {
}

void MyPolygon::Polygon::CalculateNormals() {
	if (corners.size() <= 2) {
		return;
	}
	normal = Helper::FindNormal(*corners[0], *corners[1], *corners[2]);
}
