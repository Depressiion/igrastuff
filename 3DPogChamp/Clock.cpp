#include "stdafx.h"

Clock::Clock(){
}

Clock::~Clock(){
}

void Clock::StartTimer(){
	LARGE_INTEGER li;

	if (!QueryPerformanceFrequency(&li)) {
		throw std::exception("QueryPerforamceFrequency failed");
	}
	countsPerSecond = double(li.QuadPart);

	startTimeCounts = GetCurrentCounts();
	endTimeCounts = GetCurrentCounts();
}

__int64 Clock::GetCurrentCounts() {
	LARGE_INTEGER li;
	QueryPerformanceCounter(&li);
	return li.QuadPart;
}

//in seconds
float Clock::GetTimeSinceLastTime(){
	__int64 currentCounts = GetCurrentCounts();

	__int64 timePassedSinceLastTimeCounts = currentCounts - endTimeCounts;
	endTimeCounts = currentCounts;
	return timePassedSinceLastTimeCounts / countsPerSecond;
}

float Clock::GetTotalTime(){
	return (GetCurrentCounts() - startTimeCounts) / (float)(countsPerSecond);
}
