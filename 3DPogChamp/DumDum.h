#pragma once
#include "stdafx.h"
#include "Sphere.h"

class Cylinder : public GameObject {
public:
	GLUquadric* cylinder;
	Material * mat;
	float radius, height;
	Cylinder() {};
	Cylinder(Vector3f position, float radius, float height) : radius(radius), height(height){
		this->position = position;
		cylinder = gluNewQuadric();
	}

	void Start() {};
	void Update() {

	};
	void Render() {
		glMaterialfv(GL_FRONT, GL_AMBIENT, mat->ambient);
		glMaterialfv(GL_FRONT, GL_DIFFUSE, mat->diffuse);
		glMaterialfv(GL_FRONT, GL_SPECULAR, mat->specular);
		glMaterialfv(GL_FRONT, GL_SHININESS, mat->shininess);
		glEnable(GL_LIGHTING);
		{
			glPushMatrix();
			glTranslatef(position.x, position.y, position.z);
			glRotatef(90, 1, 0, 0);
			gluCylinder(cylinder, radius, radius, height, 20, 20);
			glTranslatef(-position.x, -position.y, -position.z);
			glPopMatrix();
		}
		glDisable(GL_LIGHTING);
	};
	void SetMaterial(Material* mat) {
		this->mat = mat;
	}
};

class DumDum : public GameObject {
public:
	DumDum();
	DumDum(Vector3f position);
	~DumDum();
	Sphere* sp1;
	Sphere* sp2;
	Cylinder* cy;

	int rotAmt;

	void Start();
	void Update();
	void Render();
};

