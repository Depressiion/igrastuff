#include "stdafx.h"

RobotArm::RobotArm() {

}

RobotArm::~RobotArm() {
	delete(baseNode);
	delete(upperArmNode);
	delete(lowerArmNode);
}

void RobotArm::Start() {
	baseNodeModel = gluNewQuadric();
	lowerArm = gluNewQuadric();
	joint = gluNewQuadric();
	upperArm = gluNewQuadric();
	CreateTexture();
	BuildTree();
}

void RobotArm::BuildTree() {
	glMatrixMode(GL_MODELVIEW);

	//base
	baseNode = new TreeNode();
	baseNode->child = nullptr;
	baseNode->sibling = nullptr;
	baseNode->drawId = DrawFunction::Base;
	glLoadIdentity();
	glGetFloatv(GL_MODELVIEW_MATRIX, baseNode->matrix);

	//first joint
	lowerJointNode = new TreeNode();
	lowerJointNode->child = nullptr;
	lowerJointNode->sibling = nullptr;
	lowerJointNode->drawId = DrawFunction::Joint;

	glLoadIdentity();
	glTranslatef(0, 0.5f, 0);
	glGetFloatv(GL_MODELVIEW_MATRIX, lowerJointNode->matrix);
	for (int count = 0; count < 16; count++)
		print(lowerJointNode->matrix[count]);

	//lower arm
	lowerArmNode = new TreeNode();
	lowerArmNode->child = nullptr;
	lowerArmNode->sibling = nullptr;
	lowerArmNode->drawId = DrawFunction::LowerArm;
	glLoadIdentity();
	glTranslatef(0, 0.5f, 0);
	glGetFloatv(GL_MODELVIEW_MATRIX, lowerArmNode->matrix);

	//upper arm joint
	upperJointNode = new TreeNode();
	upperJointNode->child = nullptr;
	upperJointNode->sibling = nullptr;
	upperJointNode->drawId = DrawFunction::Joint;
	glLoadIdentity();
	glTranslatef(0, 1.5f, 0);
	glGetFloatv(GL_MODELVIEW_MATRIX, upperJointNode->matrix);

	//upper arm
	upperArmNode = new TreeNode();
	upperArmNode->child = nullptr;
	upperArmNode->sibling = nullptr;
	upperArmNode->drawId = DrawFunction::UpperArm;
	glLoadIdentity();
	glTranslatef(0, 1.5f, 0);
	glGetFloatv(GL_MODELVIEW_MATRIX, upperArmNode->matrix);

	//clear matrix
	glLoadIdentity();

	baseNode->child = lowerArmNode;
	lowerArmNode->child = upperArmNode;
	lowerArmNode->sibling = lowerJointNode;
	upperArmNode->sibling = upperJointNode;
}

void RobotArm::RenderTree(TreeNode * root) {
	if (!root) return;

	glPushMatrix();
	{
		//glMultMatrixf(root->matrix);
		RenderFunction(root->drawId);
		if (root->child) RenderTree(root->child);
	}
	glPopMatrix();

	if (root->sibling) RenderTree(root->sibling);
}

void RobotArm::RenderFunction(DrawFunction id) {
	switch (id) {
	case Base:
		RenderBase();
		break;
	case LowerArm:
		RenderLowerArm();
		break;
	case UpperArm:
		RenderUpperArm();
		break;
	case Joint:
		RenderJoint();
		break;
	default:
		break;
	}
}

void RobotArm::Render() {
	RenderTree(baseNode);
}

void RobotArm::RenderBase() {
	glPushMatrix();
	{
		glEnable(GL_TEXTURE_2D);
		glMatrixMode(GL_TEXTURE);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB,
			64, 64, 0, GL_RGB, GL_UNSIGNED_BYTE, texture);
		glTexParameterf(GL_TEXTURE_2D,
			GL_TEXTURE_WRAP_S, GL_CLAMP);
		glTexParameterf(GL_TEXTURE_2D,
			GL_TEXTURE_WRAP_T, GL_CLAMP);
		glTexParameterf(GL_TEXTURE_2D,
			GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glTexParameterf(GL_TEXTURE_2D,
			GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glColor3f(1, 1, 1);
		glMatrixMode(GL_MODELVIEW);
		glTranslatef(0, 0.5f, 0);
		glRotatef(90, 1, 0, 0);
		gluQuadricTexture(baseNodeModel, GL_TRUE);
		gluCylinder(baseNodeModel, 1, 1, 1, 64, 64);
		glDisable(GL_TEXTURE_2D);
	}
	glPopMatrix();

	float yHeightOfCap = 0.5f;
	float radius = 1;
	glColor3f(1, 0, 0);	//color
	glBegin(GL_POLYGON);
	{
		for (float count = 0; count <= 360; count += 1.0f) {
			float x = radius * cos(count);
			float z = radius * sin(count);
			glVertex3f(x, yHeightOfCap, z);
		}
	}
	glEnd();


}

void RobotArm::RenderUpperArm() {
}

void RobotArm::RenderLowerArm() {
}

void RobotArm::RenderJoint() {
	Material JointMat = {
		{ 0.4, 0.4, 0.0, 1.0 }, // Ambient
		{ 0.0, 0.9, 0.0, 1.0 }, // Diffuse
		{ 0.8, 0.8, 0.8, 1.0 }, // Specular
		32 // Shininess
	};
	glPushMatrix();
	{
		glEnable(GL_LIGHTING);

		glColor3f(1, 1, 1);
		SetMaterial(&JointMat);
		gluSphere(joint, 0.5f, 64, 64);

		glDisable(GL_LIGHTING);
	}
	glPopMatrix();
}

void RobotArm::HandleKeyDown(WPARAM wParam) {
}

void RobotArm::CreateTexture() {
	int nrOfCheckersOnRow = 8;
	float dim = 64.0 / nrOfCheckersOnRow;
	int red = 0;
	int green = 0;
	int blue = 0;
	for (int i = 0; i < 64; i++) {
		for (int j = 0; j < 64; j++) {

			// Calculate in which checkerboard
			//rectangle the pixel falls
			int row = (int)(i / dim);
			int col = (int)(j / dim);
			int c = 0;
			if (row % 2 == 0) { // Even rows start with black
				if (col % 2 == 0) {
					// All even column will be black
					red = green = blue = 0;
				}
				else {
					blue = 0;
					green = red = 255;
				}
			}
			else {
				// Odd rows start with yellow
				if (col % 2 == 0) {
					// All even column will be yellow
					green = red = 255;
					blue = 0;
				}
				else {
					red = green = blue = 0;
				}
			}
			texture[i][j][0] = (GLubyte)red;
			texture[i][j][1] = (GLubyte)green;
			texture[i][j][2] = (GLubyte)blue;
		}
	}
}

void RobotArm::SetMaterial(Material * mat) {
	glMaterialfv(GL_FRONT, GL_AMBIENT, mat->ambient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, mat->diffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, mat->specular);
	glMaterialfv(GL_FRONT, GL_SHININESS, mat->shininess);
}
