#pragma once
#include "stdafx.h"

float cubeColors[][3] = {
	0, 1, 0, // Front is Green
	1, 0, 0, // Back is Red
	0, 0, 1, // Right is Blue
	1, 1, 0, // Left is Yellow
	0, 0, 0, // Top is Black
	0.5, 0.5, 0.5 // Bottom is Grey
};

Cube::Cube(Clock& clock) : clock(clock) {
	InitCorners(0.5f);
	CreateTexture();
	//CreateGayTexture();
	//ReadBMP("LoadImg.BMP");

	State Accelerating = [](GameObject& agent) {
		Cube* cube = dynamic_cast<Cube*>(&agent);
		cube->rotationSpeed += 20 * cube->deltaTime;
		if (cube->rotationSpeed >= 360) {
			cube->internalCounter = 0;
			cube->rotationSpeed = 360;
			print(cube->clock.GetTotalTime());
			cube->GetFSM()->currentState++;
		}
	};

	State MaxVelocity = [](GameObject& agent) {
		Cube* cube = dynamic_cast<Cube*>(&agent);
		cube->internalCounter += cube->deltaTime;
		if (cube->internalCounter >= 2) {
			cube->internalCounter = 0;
			print(cube->clock.GetTotalTime());
			cube->GetFSM()->currentState++;
		}
	};

	State Deceleration = [](GameObject& agent) {
		Cube* cube = dynamic_cast<Cube*>(&agent);
		cube->rotationSpeed -= 40 * cube->deltaTime;
		if (cube->rotationSpeed <= 0) {
			cube->rotationSpeed = 0;
			print(cube->clock.GetTotalTime());
			cube->GetFSM()->currentState++;
		}
	};

	State StandStill = [](GameObject& agent) {};

	stateMachine.PushBackState(Accelerating);
	stateMachine.PushBackState(MaxVelocity);
	stateMachine.PushBackState(Deceleration);
	stateMachine.PushBackState(StandStill);
}

Cube::Cube(float size, Clock& clock) : size(size),  clock(clock){
	float radius = size / 2;
	InitCorners(radius);
	CreateTexture();
}

Cube::~Cube() {
	//for (auto obj = squares.begin(); obj != squares.end(); obj++) {
	//	for (int count = 0; count < 4; count++) {
	//		delete((*obj)->corners[count]);
	//	}
	//}
}

void Cube::Update() {
	deltaTime = clock.GetTimeSinceLastTime();
	//print(rotationAmount);
	rotationAmount += rotationSpeed * deltaTime;

	if (fsmEnabled) {
		stateMachine.Act(*this);
	}
}

void Cube::Render() {
	float faceCords[4][2] = {
		{	0, 0	},
		{	0, 1	},
		{	1, 1	},
		{	1, 0	}
	};

	glEnable(GL_CULL_FACE);
	glFrontFace(GL_CW);
	glPolygonMode(GL_FRONT, GL_FILL);
	glEnable(GL_TEXTURE_2D);
	glColor3f(1, 1, 1);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, 64, 64, 0, GL_RGB, GL_UNSIGNED_BYTE, texture);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

	glPushMatrix();
	glTranslatef(position.x, position.y, position.z);
	glRotatef(rotationAmount, 0, 1, 0);
	glTranslatef(-position.x, -position.y, -position.z);

	//glEnable(GL_LIGHTING);

	for (int count = 0; count < squares.size(); count++) {
		glBegin(GL_QUADS);
		{
			//glColor3f(cubeColors[count][0], cubeColors[count][1], cubeColors[count][2]);
			Vector3f normal = squares[count]->normal;
			glNormal3f(normal.x, normal.y, normal.z);

			for (int v = 0; v < 4; v++) {
				Vector3f temp = *squares[count]->corners[v];
				glTexCoord2f(faceCords[v][0], faceCords[v][1]);
				glVertex3f(temp.x, temp.y, temp.z);
			}
		}
		glEnd();
	}//end loop

	glDisable(GL_LIGHTING);

	//draw normal
	//for (int count = 0; count < squares.size(); count++) {
	//	//Helper::DrawNormal(*squares[count]);
	//}

	glPopMatrix();
	glDisable(GL_TEXTURE_2D);
}

void Cube::InitCorners(float radius) {
	std::vector<Vector3f*> pointsOnCube;
	pointsOnCube.clear();	//just in case

	for (float x = -radius; x <= radius; x += radius * 2) {
		for (float y = -radius; y <= radius; y += radius * 2) {
			for (float z = -radius; z <= radius; z += radius * 2) {
				pointsOnCube.push_back(new Vector3f(position.x + x, position.y + y, position.z + z));
			}
		}
	}

	InitSquares(pointsOnCube);
}

void Cube::InitSquares(std::vector<Vector3f*> pointsOnCube) {
	squares.clear();	//make sure only got 6 sides

	//squares.push_back(new Square(pointsOnCube[0], pointsOnCube[2], pointsOnCube[3], pointsOnCube[1], Vector3f(-1, 0, 0)));	//left face
	//squares.push_back(new Square(pointsOnCube[4], pointsOnCube[5], pointsOnCube[7], pointsOnCube[6], Vector3f(1, 0, 0)));	//right face

	//squares.push_back(new Square(pointsOnCube[6], pointsOnCube[7], pointsOnCube[3], pointsOnCube[2], Vector3f(0, 1, 0)));	//top face
	//squares.push_back(new Square(pointsOnCube[4], pointsOnCube[5], pointsOnCube[1], pointsOnCube[0], Vector3f(0, -1, 0)));	//down face

	//squares.push_back(new Square(pointsOnCube[7], pointsOnCube[5], pointsOnCube[1], pointsOnCube[3], Vector3f(0, 0, 1)));	//back face
	//squares.push_back(new Square(pointsOnCube[2], pointsOnCube[0], pointsOnCube[4], pointsOnCube[6], Vector3f(0, 0, -1)));//front face


	squares.push_back(new Square(pointsOnCube[0], pointsOnCube[2], pointsOnCube[3], pointsOnCube[1]));	//left face
	squares.push_back(new Square(pointsOnCube[4], pointsOnCube[5], pointsOnCube[7], pointsOnCube[6]));	//right face

	squares.push_back(new Square(pointsOnCube[6], pointsOnCube[7], pointsOnCube[3], pointsOnCube[2]));	//top face
	squares.push_back(new Square(pointsOnCube[4], pointsOnCube[5], pointsOnCube[1], pointsOnCube[0]));	//down face

	squares.push_back(new Square(pointsOnCube[7], pointsOnCube[5], pointsOnCube[1], pointsOnCube[3]));	//back face
	squares.push_back(new Square(pointsOnCube[2], pointsOnCube[0], pointsOnCube[4], pointsOnCube[6]));//front face

	for (int count = 0; count < squares.size(); count++) {
		squares[count]->CalculateNormals();
	}
}

void Cube::CreateTexture() {
	int noOfSquares = 8;
	float dim = 64 / noOfSquares;
	bool isBlack = false;
	for (int x = 0; x < 64; x++) {
		if (x % noOfSquares == 0) {
			isBlack = !isBlack;
		}
		for (int y = 0; y < 64; y++) {
			if (y % noOfSquares == 0) {
				isBlack = !isBlack;
			}

			if (x == 0 || y == 0 || y == 63 || x == 63) {
				texture[x][y][0] = (GLubyte)0;
				texture[x][y][1] = (GLubyte)255;
				texture[x][y][2] = (GLubyte)0;
				continue;
			}   //outline

			if (isBlack) {
				texture[x][y][0] = (GLubyte)0;
				texture[x][y][1] = (GLubyte)0;
				texture[x][y][2] = (GLubyte)0;
			}
			else {
				texture[x][y][0] = (GLubyte)255;
				texture[x][y][1] = (GLubyte)0;
				texture[x][y][2] = (GLubyte)0;
			}


		}
	}
}

void Cube::CreateGayTexture() {
	int noOfSquares = 8;
	float dim = 64 / noOfSquares;

	for (int x = 0; x < 64; x++) {
		for (int y = 0; y < 64; y++) {
			if (x == 0 || y == 0 || x == 63 || y == 63) {
				texture[x][y][0] = (GLubyte)0;
				texture[x][y][1] = (GLubyte)0;
				texture[x][y][2] = (GLubyte)0;
				continue;
			}//outline

			float col = (int)(x / noOfSquares);
			float row = (int)(y / noOfSquares);

			//when red
			if (col == 0 || row == 0 || col == noOfSquares - 1 || row == noOfSquares - 1) {
				//red
				texture[x][y][0] = (GLubyte)255;
				texture[x][y][1] = (GLubyte)0;
				texture[x][y][2] = (GLubyte)0;
			}
			else if (col == 1 || row == 1 || col == noOfSquares - 2 || row == noOfSquares - 2) {
				//gr
				texture[x][y][0] = (GLubyte)0;
				texture[x][y][1] = (GLubyte)255;
				texture[x][y][2] = (GLubyte)0;
			}
			else if (col == 2 || row == 2 || col == noOfSquares - 3 || row == noOfSquares - 3) {
				//gr
				texture[x][y][0] = (GLubyte)0;
				texture[x][y][1] = (GLubyte)0;
				texture[x][y][2] = (GLubyte)255;
			}
			else {
				//gr
				texture[x][y][0] = (GLubyte)200;
				texture[x][y][1] = (GLubyte)200;
				texture[x][y][2] = (GLubyte)0;
			}
		}
	}
}

void Cube::ReadBMP(const char * str) {
	FILE * file = std::fopen(str, "rb");

	//info
	unsigned char info[54];
	fread(info, sizeof(unsigned char), 54, file);
	int width = *(int*)&info[18];
	int height = *(int*)&info[22];
	int size = (width * 3 + 3) & (~3);
	unsigned char* data = new unsigned char[size];

	//for (int x = 0; x < 64; x++) {
	//	for (int y = 0; y < 64; y++) {
	//		for (int count = 0; count < 3; count++) {
	//			texture[x][y][count] = data[3 * (x * width + y) + count];
	//		}
	//	}
	//}

	for (int x = 0; x < height; x++) {
		fread(data, sizeof(unsigned char), size, file);
		for (int y = 0; y < width * 3; y++) {
			data[y] ^= data[y + 2];
			data[y + 2] ^= data[y];
			data[y] ^= data[y + 2];

			texture[x][y][0] = data[y];
			texture[x][y][1] = data[y + 1];
			texture[x][y][2] = data[y + 2];
		}
	}

	fclose(file);
}
