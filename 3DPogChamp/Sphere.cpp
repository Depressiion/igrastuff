#include "stdafx.h"

Sphere::Sphere() {
	quad = gluNewQuadric();
}

Sphere::Sphere(Vector3f pos, float radius) : radius(radius){
	quad = gluNewQuadric();
	this->position = pos;
}

Sphere::~Sphere() {
}

void Sphere::Update() {

}

void Sphere::Render() {
	glMaterialfv(GL_FRONT, GL_AMBIENT, mat->ambient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, mat->diffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, mat->specular);
	glMaterialfv(GL_FRONT, GL_SHININESS, mat->shininess);
	glEnable(GL_LIGHTING);
	glPushMatrix();
	glTranslatef(position.x, position.y, position.z);
	gluSphere(quad, radius, 20, 20);
	glPopMatrix();
	glDisable(GL_LIGHTING);
}

void Sphere::SetMaterial(Material * material) {
	this->mat = material;
}
