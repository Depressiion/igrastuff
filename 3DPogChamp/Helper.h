#pragma once
#include "stdafx.h"
#include "Polygon.h"
struct Square;


class Helper {
public:
	Helper();
	~Helper();

public:
	static void Draw3DAxis();
	static void DrawNormal(MyPolygon::Polygon);
	static Vector3f FindNormal(Vector3f v1, Vector3f v2, Vector3f v3);
};

//template <class T> where T : Polygon
//static T SortPolygon(T input)
//{
//
//}

