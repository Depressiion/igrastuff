#pragma once
namespace MyPolygon {
	class Polygon {
	public:
		Polygon();
		Polygon(Vector3f normal);
		virtual ~Polygon();
		std::vector<Vector3f*> corners;
		Vector3f normal;
		int noOfCorners;

		void CalculateNormals();
	};
}

