#pragma once
#define W_WIDTH 400
#define W_HEIGHT 400
#define E_WIDTH 17
#define E_HEIGHT 50
#include "resource.h"

static PIXELFORMATDESCRIPTOR pfd{
	 sizeof(PIXELFORMATDESCRIPTOR),
	 1,// Version Number
	 PFD_DRAW_TO_WINDOW | // Format Must Support Window
	 PFD_SUPPORT_OPENGL | // Format Must Support OpenGL
	 PFD_DOUBLEBUFFER,// Must Support Double Buffering
	 PFD_TYPE_RGBA, // Request An RGBA Format
	 16, // Select Our Color Depth = 16
	 0, 0, 0, 0, 0, 0, // Color Bits Ignored
	 0,// No Alpha Buffer
	 0,// Shift Bit Ignored
	 0,// No Accumulation Buffer
	 0, 0, 0, 0, // Accumulation Bits Ignored
	 32, // 32 Bit Z-Buffer (Depth Buffer)
	 0,// No Stencil Buffer
	 0,// No Auxiliary Buffer
	 PFD_MAIN_PLANE,// Main Drawing Layer
	 0,// Reserved
	 0, 0, 0// Layer Masks Ignored
};


