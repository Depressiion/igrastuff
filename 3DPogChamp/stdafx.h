// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//
#pragma once

#include "targetver.h"
#define print(x)std::cout << x << std::endl;

#define WIN32_LEAN_AND_MEAN             // Exclude rarely-used stuff from Windows headers
// Windows Header Files
#include <windows.h>

// C RunTime Header Files
#include <stdlib.h>
#include <malloc.h>
#include <memory.h>
#include <tchar.h>


// reference additional headers your program requires here
#include <iostream>
#include <thread>
#include <vector>
#include <fstream>
#include <gl/GL.h>
#include <gl/GLU.h>

typedef struct matstruct {
	GLfloat ambient[4];
	GLfloat diffuse[4];
	GLfloat specular[4];
	GLfloat shininess[4];
} Material;

#include "Vertex3f.h"
#include "Polygon.h"
#include "Clock.h"
#include "FSM.h"
#include "GameObject.h"
#include "3DPogChamp.h"
#include "Helper.h"
#include "RobotArm.h"
#include "Cube.h"
#include "Sphere.h"
#include "DumDum.h"
#include "Prymid.h"


using namespace std;

/* print matrix

	for (int count = 0; count < 4; count++)
		std::cout << baseNode->matrix[0 + count] << baseNode->matrix[1 + count] << baseNode->matrix[2 + count] << baseNode->matrix[3 + count] << "\n";
	print("");

*/


