#pragma once
#include "stdafx.h"

class Sphere : public GameObject{
public:
	Sphere();
	Sphere(Vector3f pos, float radius);
	~Sphere();
	float radius;
	Material* mat;
	GLUquadric* quad;

	void Update();
	virtual void Render();
	void SetMaterial(Material* material);
};

class SphereWithFor : public GameObject {
public:
	SphereWithFor(Vector3f pos, int noOfBall, float initialRadius) {
		this->position = pos;

		for (int count = 0; count < noOfBall; count++) {
			spheres.push_back(new Sphere(pos + Vector3f(initialRadius / 2 + initialRadius, 0, 0), initialRadius / 2));
			spheres.push_back(new Sphere(pos + Vector3f(0, initialRadius / 2 + initialRadius, 0), initialRadius / 2));
			spheres.push_back(new Sphere(pos + Vector3f(0, 0, initialRadius / 2 + initialRadius), initialRadius / 2));
			initialRadius /= 2;
		}
	};
	~SphereWithFor() {};

	std::vector<Sphere*> spheres;

	void Update() {};
	void Render() {
		for (int count = 0; count < spheres.size(); count++) {
			spheres[count]->Render();
		}
	};
};

class LinkedSphere : public Sphere {
public:
	enum Direction {Up, Forward, Right };
	Direction currentDir = Direction::Up;

	LinkedSphere(LinkedSphere* parent) : parent(parent){
		//position would be diameter of all parents + parent radius / 2
		currentDir = parent->currentDir;
		this->radius = parent->radius / 2;
		SetPos();
	}

	//original
	LinkedSphere(Sphere* parent, Direction dire) : currentDir(dire){
		//position would be diameter of all parents + parent radius / 2
		this->radius = parent->radius / 2;
		position += Vector3f(
			parent->position.x + currentDir == Direction::Right ? parent->radius + this->radius : 0,
			parent->position.y + currentDir == Direction::Up ? parent->radius + this->radius : 0,
			parent->position.z + currentDir == Direction::Forward ? parent->radius + this->radius : 0
		);
	}
	~LinkedSphere() {};

	LinkedSphere* parent;

	void SetPos() {
		position = Vector3f(
			parent->position.x + (currentDir == Direction::Right ? parent->radius + this->radius : 0),
			parent->position.y + (currentDir == Direction::Up ? parent->radius + this->radius : 0),
			parent->position.z + (currentDir == Direction::Forward ? parent->radius + this->radius : 0)
		);
	}
};

class SphereControl : public GameObject{
public:
	SphereControl(Vector3f pos, int noOfCubes) : noOfCubes(noOfCubes){
		this->position = pos;
		origin = new Sphere(position, 1);

	}
	~SphereControl() {};
	int noOfCubes = 4;
	std::vector<LinkedSphere*> spheres;
	Sphere* origin;

	void Start() {
		spheres.push_back(new LinkedSphere(origin, LinkedSphere::Direction::Up));
		for (int count = 0; count < noOfCubes; count++) {
			spheres.push_back(new LinkedSphere(spheres[spheres.size() - 1]));
		}

		spheres.push_back(new LinkedSphere(origin, LinkedSphere::Direction::Right));
		for (int count = 0; count < noOfCubes; count++) {
			spheres.push_back(new LinkedSphere(spheres[spheres.size() - 1]));
		}

		spheres.push_back(new LinkedSphere(origin, LinkedSphere::Direction::Forward));
		for (int count = 0; count < noOfCubes; count++) {
			spheres.push_back(new LinkedSphere(spheres[spheres.size() - 1]));
		}
	}
	void Update() {
		for (int count = 0; count < spheres.size(); count++) {
			spheres[count]->Update();
		}
	}
	void Render() {
		if (enabled) {
			origin->Render();
			//render all
			for (int count = 0; count < spheres.size(); count++) {
				spheres[count]->Render();
			}
		}
	}

	void SetMaterial(Material* mat) {
		origin->SetMaterial(mat);
		for (int count = 0; count < spheres.size(); count++) {
			spheres[count]->SetMaterial(mat);
		}
	}

};

