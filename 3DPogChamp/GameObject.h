#pragma once
class GameObject {
public:
	GameObject();
	virtual ~GameObject();
	Vector3f position;
	Vector3f rotation;
	bool enabled = true;

	virtual void Update() = 0;
	virtual void Render() = 0;
	virtual FSM* GetFSM() { return nullptr; }
};

