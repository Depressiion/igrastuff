#pragma once
#include "stdafx.h"
Clock cl;

Cube cube(cl);
//Prymid p1, p2;
//Sphere sp;
DumDum dum(Vector3f(0, 0, 0));
SphereControl manyBal(Vector3f(0, 0, 0), 2);
CircleHourGlass chg(Vector3f(0, 0, 0), 3);
//SphereWithFor swf(Vector3f(0, 0, 0), 4, 0.5f);
Prymid p;
RobotArm ra;

int InitOpenGL();
void DrawGLScene();
void ReSizeGLScene(GLsizei width, GLsizei height);
void Update();
Vector3f mousePos;
int noOfFrames = 0;

GLuint PixelFormat;
HDC hDC = NULL;
HWND hWnd = NULL;
HGLRC hRC = NULL;

#define MAX_LOADSTRING 100

// Global Variables:
HINSTANCE hInst;                                // current instance
WCHAR szTitle[MAX_LOADSTRING];                  // The title bar text
WCHAR szWindowClass[MAX_LOADSTRING];            // the main window class name

// Forward declarations of functions included in this code module:
ATOM                MyRegisterClass(HINSTANCE hInstance);
BOOL                InitInstance(HINSTANCE, int);
LRESULT CALLBACK    WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK    About(HWND, UINT, WPARAM, LPARAM);

int APIENTRY wWinMain(_In_ HINSTANCE hInstance,
	_In_opt_ HINSTANCE hPrevInstance,
	_In_ LPWSTR    lpCmdLine,
	_In_ int       nCmdShow) {
	UNREFERENCED_PARAMETER(hPrevInstance);
	UNREFERENCED_PARAMETER(lpCmdLine);

	// TODO: Place code here.
	AllocConsole();	//open console
	freopen("conin$", "r", stdin);	//read console input
	freopen("conout$", "w", stdout);	//write to file
	freopen("conout$", "w", stderr);
	printf("Debugging Window:\n");
	//console

	//clock
	cl.StartTimer();

	dum.enabled = false;
	manyBal.enabled = false;
	chg.enabled = false;
	cube.enabled = false;
	p.enabled = false;

	p.position = Vector3f(0, 0, 0);
	p.heightMultiplier = 1.5f;

	if(p.enabled) p.Start();
	//ra.Start();

	//prymid 1
	//top one
	//p1.heightMultiplier = -3;
	//p1.position = Vector3f(0, 1.5f, 0);
	//p1.rotation = Vector3f(0, -1, 0);

	//prymid 2
	//p2.heightMultiplier = 2;
	//p2.position = Vector3f(0, 0, 0);
	//p2.rotation = Vector3f(0, 1, 0);
	//p2.rotationAmout = 180;

	//if (manyBal.enabled) {
	//	manyBal.Start();
	//	manyBal.SetMaterial(&redPlasticMaterial);
	//}

	//if (dum.enabled) {
	//	dum.sp1->SetMaterial(&redPlasticMaterial);
	//	dum.sp2->SetMaterial(&redPlasticMaterial);
	//	dum.cy->SetMaterial(&greenMat);

	//	dum.rotation = Vector3f(1, 0, 0);
	//}

	if (chg.enabled) {
		chg.Start();
	}

	//sp.SetMaterial(&redPlasticMaterial);

	// Initialize global strings
	LoadStringW(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
	LoadStringW(hInstance, IDC_MY3DPOGCHAMP, szWindowClass, MAX_LOADSTRING);
	MyRegisterClass(hInstance);

	// Perform application initialization:
	if (!InitInstance(hInstance, nCmdShow)) {
		return FALSE;
	}

	HACCEL hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_MY3DPOGCHAMP));

	MSG msg;

	// Main message loop:
	while (GetMessage(&msg, nullptr, 0, 0)) {
		if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg)) {
			TranslateMessage(&msg);
			DispatchMessage(&msg);
			DrawGLScene();
			SwapBuffers(hDC);
		}
	}

	return (int)msg.wParam;
}



//
//  FUNCTION: MyRegisterClass()
//
//  PURPOSE: Registers the window class.
//
ATOM MyRegisterClass(HINSTANCE hInstance) {
	WNDCLASSEXW wcex;

	wcex.cbSize = sizeof(WNDCLASSEX);

	wcex.style = CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc = WndProc;
	wcex.cbClsExtra = 0;
	wcex.cbWndExtra = 0;
	wcex.hInstance = hInstance;
	wcex.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_MY3DPOGCHAMP));
	wcex.hCursor = LoadCursor(nullptr, IDC_ARROW);
	wcex.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
	wcex.lpszMenuName = MAKEINTRESOURCEW(IDC_MY3DPOGCHAMP);
	wcex.lpszClassName = szWindowClass;
	wcex.hIconSm = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

	return RegisterClassExW(&wcex);
}

//
//   FUNCTION: InitInstance(HINSTANCE, int)
//
//   PURPOSE: Saves instance handle and creates main window
//
//   COMMENTS:
//
//        In this function, we save the instance handle in a global variable and
//        create and display the main program window.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow) {
	hInst = hInstance; // Store instance handle in our global variable

	//hWnd = CreateWindowW(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW,
	//	CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, nullptr, nullptr, hInstance, nullptr);

	hWnd = CreateWindowW(szWindowClass,
		szTitle, WS_OVERLAPPEDWINDOW,
		0, 0, W_WIDTH + E_WIDTH, W_HEIGHT + E_HEIGHT,
		nullptr, nullptr, hInstance, nullptr);

	InitOpenGL();

	if (!hWnd) {
		return FALSE;
	}

	ShowWindow(hWnd, nCmdShow);
	UpdateWindow(hWnd);

	//start timer
	SetTimer(hWnd, 0, 1, NULL);
	ReSizeGLScene(W_WIDTH + E_WIDTH, W_HEIGHT + E_HEIGHT);

	return TRUE;
}

//
//  FUNCTION: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  PURPOSE: Processes messages for the main window.
//
//  WM_COMMAND  - process the application menu
//  WM_PAINT    - Paint the main window
//  WM_DESTROY  - post a quit message and return
//
//
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam) {
	switch (message) {
	case WM_COMMAND:
	{
		int wmId = LOWORD(wParam);
		// Parse the menu selections:
		switch (wmId) {
		case IDM_ABOUT:
			DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);
			break;
		case IDM_EXIT:
			DestroyWindow(hWnd);
			break;
		default:
			return DefWindowProc(hWnd, message, wParam, lParam);
		}
	}
	break;
	case WM_PAINT:
	{
		PAINTSTRUCT ps;
		HDC hdc = BeginPaint(hWnd, &ps);
		// TODO: Add any drawing code that uses hdc here...
		EndPaint(hWnd, &ps);
	}
	break;
	case WM_TIMER:
	{
		Update();
	}
	break;
	case WM_SIZE:
	{
		ReSizeGLScene(LOWORD(lParam), HIWORD(lParam));
		break;
	}
	case WM_KEYDOWN:
	{
		switch (HIWORD(lParam)) {
		case 17:
			//moveForward = true;
			break;
		default: break;
		}

		break;
	}
	case WM_KEYUP:
	{
		switch (HIWORD(lParam)) {
		case 17:
			break;
		default: break;
		}
		break;
	}
	case WM_MOUSEMOVE:
	{
		mousePos = Vector3f(LOWORD(lParam), HIWORD(lParam));
	}
	break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
	}
	return 0;
}

// Message handler for about box.
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam) {
	UNREFERENCED_PARAMETER(lParam);
	switch (message) {
	case WM_INITDIALOG:
		return (INT_PTR)TRUE;

	case WM_COMMAND:
		if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL) {
			EndDialog(hDlg, LOWORD(wParam));
			return (INT_PTR)TRUE;
		}
		break;
	}
	return (INT_PTR)FALSE;
}

void Update() {

	//p1.Update();
	//p2.Update();

	if(cube.enabled) cube.Update();
	if(manyBal.enabled) manyBal.Update();
	if(chg.enabled) chg.Update();
}

//added stuff
void DrawGLScene() {

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glClearColor(1, 1, 1, 1);

	// Locate camera in the (1,1,1) position then look at
	// origin (the intersection of the axis system)
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	gluLookAt(3, 3, 3, // Camera's position
		0, 0, 0, // Camera�s target to look at
		0, 1, 0); // Orientation of camera

	//sp.Render();
	//p2.Render();
	//p2.RenderWireframe();
	//ra.Render();
	//ra.Render();

	if(cube.enabled) cube.Render();
	if(dum.enabled) dum.Render();
	if(manyBal.enabled) manyBal.Render();
	if (chg.enabled) chg.Render();
	if (p.enabled)  p.RenderOneFace();
	//swf.Render();
	Helper::Draw3DAxis();

}

int InitOpenGL() {
	auto SetupLight = [] {
		glShadeModel(GL_SMOOTH);
		GLfloat LightAmbient[] = { 0.5, 0.5, 0.5, 1 };
		GLfloat LightDiffuse[] = { 0.5, 0.5, 0.5, 1 };
		GLfloat LightSpecular[] = { 0.5, 0.5, 0.5, 1 };
		GLfloat LightPosition[] = { 10, 10, 10, 0 };
		glLightfv(GL_LIGHT0, GL_AMBIENT, LightAmbient);
		glLightfv(GL_LIGHT0, GL_DIFFUSE, LightDiffuse);
		glLightfv(GL_LIGHT0, GL_SPECULAR, LightSpecular);
		glLightfv(GL_LIGHT0, GL_POSITION, LightPosition);
		glEnable(GL_LIGHT0);
	};

	// Get Device Dontext
	if (!(hDC = GetDC(hWnd))) {
		MessageBox(NULL, L"Can't Create A GL Device Context.",
			L"ERROR", MB_OK | MB_ICONEXCLAMATION);
		return 0;
	}
	// Check if Windows can find a matching Pixel Format
	if (!(PixelFormat = ChoosePixelFormat(hDC, &pfd))) {
		MessageBox(NULL, L"Can't Find A Suitable PixelFormat.",
			L"ERROR", MB_OK | MB_ICONEXCLAMATION);
		return 0;
	}
	// Try to set pixel format
	if (!SetPixelFormat(hDC, PixelFormat, &pfd)) {
		MessageBox(NULL, L"Can't Set The PixelFormat.", L"ERROR",
			MB_OK | MB_ICONEXCLAMATION);
		return 0;
	}
	// Get a Rendering context
	if (!(hRC = wglCreateContext(hDC))) {
		MessageBox(NULL, L"Can't Create A GL Rendering Context.",
			L"ERROR", MB_OK | MB_ICONEXCLAMATION);
		return 0;
	} // Activate Rendering Context
	if (!wglMakeCurrent(hDC, hRC)) {
		MessageBox(NULL, L"Can't Activate The GL Rendering Context.", L"ERROR", MB_OK | MB_ICONEXCLAMATION);
		return 0;
	}

	// TO SOLVE DEPTH-BUFFER ISSUE
	glClearDepth(1.0f);

	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
	SetupLight();
	return 1;
}

void ReSizeGLScene(GLsizei width, GLsizei height) {
	if (height == 0)// Prevent A Divide By Zero By
		height = 1; // Making Height Equal One
		// Reset The Current Viewport
	glViewport(0, 0, width, height);
	// Select The Projection Matrix
	glMatrixMode(GL_PROJECTION);
	// Reset The Projection Matrix
	glLoadIdentity();
	// Calculate The Aspect Ratio Of The Window
	gluPerspective(45.0f, (GLfloat)width / (GLfloat)height,
		0.1f, 100.0f);

}


