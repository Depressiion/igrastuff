#pragma once
#include "stdafx.h"

Vector3f prymidColors[] = {
	Vector3f(1, 0, 0),	//red
	Vector3f(0, 1, 0),		//g
	Vector3f(0, 0, 1),
	Vector3f(1, 1, 0),
	Vector3f(0, 0, 0),
	Vector3f(0, 0, 0)
};

Prymid::Prymid() {

}

Prymid::~Prymid() {

}

void Prymid::Start() {
	Vector3f* null = new Vector3f(0, 0, 0);
	for (int count = 0; count < 6; count++) {
		triangles.push_back(new Triangle(null, null, null));
	}
	InitCorners(r);
	CreateTexture();
}

void Prymid::Update() {
	this->InitCorners(0.5f);
	++rotationAmout %= 360;
	//RenderWireframe();
}

void Prymid::Render() {
	glEnable(GL_LIGHTING);
	glEnable(GL_CULL_FACE);
	glFrontFace(GL_CW);
	glPolygonMode(GL_FRONT, GL_FILL);

	glPushMatrix();
	glTranslatef(position.x, position.y, position.z);
	glRotatef(rotationAmout, rotation.x, rotation.y, rotation.z);
	glTranslatef(-position.x, -position.y, -position.z);

	GLfloat grey[] = { 0.5f, 0.5f, 0.5f, 0 };
	glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, grey);

	for (int count = 0; count < 6; count++) {
		glBegin(GL_TRIANGLES);
		{
			Vector3f normal = triangles[count]->normal;
			glNormal3f(normal.x, normal.y, normal.z);
			//glColor3f(prymidColors[count].x, prymidColors[count].y, prymidColors[count].z);
			for (int v = 0; v < 3; v++) {
				Vector3f temp = *triangles[count]->corners[v];
				glVertex3f(temp.x, temp.y, temp.z);
			}

		}
		glEnd();
	}
	glDisable(GL_LIGHTING);

	for (int count = 0; count < triangles.size(); count++) {
		Helper::DrawNormal(*triangles[count]);
	}

	glPopMatrix();


}

void Prymid::RenderNoMat() {
	//glEnable(GL_CULL_FACE);
	glFrontFace(GL_CW);
	glPolygonMode(GL_FRONT, GL_FILL);

	glPushMatrix();
	glTranslatef(position.x, position.y, position.z);
	for (int count = 0; count < rotations.size(); count++) {
		glRotatef(rotationAmounts[count], rotations[count].x, rotations[count].y, rotations[count].z);
	}
	glTranslatef(-position.x, -position.y, -position.z);

	for (int count = 0; count < 6; count++) {
		glBegin(GL_TRIANGLES);
		{
			glColor3f(prymidColors[count].x, prymidColors[count].y, prymidColors[count].z);
			for (int v = 0; v < 3; v++) {
				Vector3f temp = *triangles[count]->corners[v];
				glVertex3f(temp.x, temp.y, temp.z);
			}

		}
		glEnd();
	}
	glPopMatrix();
}

void Prymid::RenderWireframe() {
	glEnable(GL_CULL_FACE);
	glFrontFace(GL_CW);
	glPolygonMode(GL_FRONT, GL_FILL);

	glPushMatrix();
	glTranslatef(position.x, position.y, position.z);
	glRotatef(rotationAmout, rotation.x, rotation.y, rotation.z);
	glTranslatef(-position.x, -position.y, -position.z);

	for (int count = 0; count < triangles.size(); count++) {
		glBegin(GL_TRIANGLES);
		{
			glColor3f(0.6f, 0.6f, 0.6f);
			for (int v = 0; v < 3; v++) {
				Vector3f temp = *triangles[count]->corners[v];
				glVertex3f(temp.x, temp.y, temp.z);
			}
		}
		glEnd();

		glBegin(GL_LINE_LOOP);
		{
			glColor3f(0, 0, 0);
			for (int v = 0; v < 3; v++) {
				Vector3f temp = *triangles[count]->corners[v];
				glVertex3f(temp.x, temp.y, temp.z);
			}
		}
		glEnd();
	}

	for (int count = 0; count < triangles.size(); count++) {
		Helper::DrawNormal(*triangles[count]);
	}

	glPopMatrix();
}

void Prymid::CreateTexture(){
	int noOfSquares = 8;
	float dim = 64 / noOfSquares;
	bool isBlack = false;
	for (int x = 0; x < 64; x++) {
		if (x % noOfSquares == 0) {
			isBlack = !isBlack;
		}
		for (int y = 0; y < 64; y++) {
			if (y % noOfSquares == 0) {
				isBlack = !isBlack;
			}

			if (x == 0 || y == 0 || y == 63 || x == 63) {
				texture[x][y][0] = (GLubyte)0;
				texture[x][y][1] = (GLubyte)255;
				texture[x][y][2] = (GLubyte)0;
				continue;
			}   //outline

			if (isBlack) {
				texture[x][y][0] = (GLubyte)0;
				texture[x][y][1] = (GLubyte)0;
				texture[x][y][2] = (GLubyte)0;
			}
			else {
				texture[x][y][0] = (GLubyte)255;
				texture[x][y][1] = (GLubyte)0;
				texture[x][y][2] = (GLubyte)0;
			}


		}
	}
}

void Prymid::RenderOneFace(){
	float faceCords[3][2] = {
		{	0, 0	},
		{	0.5f, 1	},
		{	1, 0	},
	};

	glEnable(GL_CULL_FACE);
	glFrontFace(GL_CW);
	glPolygonMode(GL_FRONT, GL_FILL);
	glEnable(GL_TEXTURE_2D);
	glColor3f(1, 1, 1);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, 64, 64, 0, GL_RGB, GL_UNSIGNED_BYTE, texture);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

	glBegin(GL_TRIANGLES);
	{
		for (int v = 0; v < 3; v++) {
			Vector3f temp = *triangles[0]->corners[v];
			glTexCoord2f(faceCords[v][0], faceCords[v][1]);
			glVertex3f(temp.x, temp.y, temp.z);
		}
	}
	glEnd();
	glDisable(GL_TEXTURE_2D);

}

void Prymid::InitCorners(float radius) {
	std::vector<Vector3f*> corners;
	corners.clear();
	//origin at bottom corner
	corners.push_back(new Vector3f(position.x, position.y + radius * heightMultiplier, position.z));	//tipy tip
	for (float x = -radius; x <= radius; x += radius * 2) {
		for (float z = -radius; z <= radius; z += radius * 2) {
			corners.push_back(new Vector3f(position.x + x, position.y, position.z + z));//corners
		}
	}
	InitTriangles(corners);
}

void Prymid::InitTriangles(std::vector<Vector3f*> corners) {
	//std::vector<Triangle*> temp = triangles;	
	triangles[0]->corners[0] = corners[2];
	triangles[0]->corners[1] = corners[0];
	triangles[0]->corners[2] = corners[4];

	triangles[1]->corners[0] = corners[1];
	triangles[1]->corners[1] = corners[0];
	triangles[1]->corners[2] = corners[2];

	triangles[2]->corners[0] = corners[4];
	triangles[2]->corners[1] = corners[0];
	triangles[2]->corners[2] = corners[3];

	triangles[3]->corners[0] = corners[3];
	triangles[3]->corners[1] = corners[0];
	triangles[3]->corners[2] = corners[1];

	//bottom
	triangles[4]->corners[0] = corners[4];
	triangles[4]->corners[1] = corners[1];
	triangles[4]->corners[2] = corners[2];

	triangles[5]->corners[0] = corners[4];
	triangles[5]->corners[1] = corners[3];
	triangles[5]->corners[2] = corners[1];


	//triangles[0] = new Triangle(corners[4], corners[0], corners[2]);
	//triangles[1] = new Triangle(corners[2], corners[0], corners[1]);
	//triangles[2] = new Triangle(corners[3], corners[0], corners[4]);
	//triangles[3] = new Triangle(corners[1], corners[0], corners[3]);
	////bottom layer
	//triangles[4] = new Triangle(corners[2], corners[1], corners[4]);
	//triangles[5] = new Triangle(corners[1], corners[3], corners[4]);

	for (int count = 0; count < triangles.size(); count++) {
		triangles[count]->CalculateNormals();
	}

	//clear space
	//for (int count = 0; count < temp.size(); count++) {
	//	delete(temp[count]);
	//}
}
