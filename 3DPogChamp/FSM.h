#pragma once
#include "stdafx.h"
class GameObject;
typedef void(*State) (GameObject& agent);

class FSM {
public:
	FSM();
	virtual ~FSM();

	std::vector<State> states;
	int currentState = 0;

	void PushBackState(State state) {
		states.push_back(state);
	};

	void Act(GameObject & agent) {
		(states[currentState])(agent);
	}
};

