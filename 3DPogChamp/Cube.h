#pragma once
#include "stdafx.h"

struct Square : public MyPolygon::Polygon {
	Square(Vector3f* topRight, Vector3f* topLeft, Vector3f* bottomRight, Vector3f* bottomLeft){
		corners.clear();
		corners.push_back(topRight);
		corners.push_back(topLeft);
		corners.push_back(bottomRight);
		corners.push_back(bottomLeft);
	}
};

class Cube : public GameObject {
public:
	Cube(Clock&);
	Cube(float size, Clock&);
	virtual ~Cube();
	float size = 1;
	float rotationAmount = 0;
	float rotationSpeed = 0;
	float deltaTime = 0;
	float internalCounter = 0;
	std::vector<Square*> squares;
	GLubyte texture[64][64][3];
	Clock & clock;
	FSM stateMachine;
	bool fsmEnabled = true;

	void Update();
	void Render();
	void InitCorners(float radius);
	void InitSquares(std::vector<Vector3f*> pointsOnCube);
	void CreateTexture();
	void CreateGayTexture();
	void ReadBMP(const char* str);

	FSM* GetFSM() {
		return &stateMachine;
	};
};


