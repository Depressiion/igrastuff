#pragma once
#include "stdafx.h"
#include "Sphere.h"
class Cylinder;
DumDum::DumDum() {
}

DumDum::DumDum(Vector3f position) {
	this->position = position;
	sp1 = new Sphere(Vector3f(0, 2, 0), 0.5f);
	sp2 = new Sphere(Vector3f(0, -2, 0), 0.5f);
	cy = new Cylinder(Vector3f(0, 2, 0), 0.3f, 4);
}

DumDum::~DumDum() {
}

void DumDum::Start() {

}

void DumDum::Update() {
	++rotAmt %= 360;
}

void DumDum::Render() {
	if (enabled) {
		glPushMatrix();
		{
			glRotatef(rotAmt, rotation.x, rotation.x, rotation.y);
			sp1->Render();
			sp2->Render();
			cy->Render();
		}
		glPopMatrix();
	}
}
