#include "stdafx.h"

Vector3f::Vector3f() {
}

Vector3f::Vector3f(float x, float y) : x(x), y(y), z(0) {
}

Vector3f::Vector3f(float x, float y, float z) : x(x), y(y), z(z) {
}

Vector3f::~Vector3f() {
}

Vector3f Vector3f::getCross(Vector3f vec1, Vector3f vec2) {
	return Vector3f(vec1.y * vec2.z - vec1.z * vec2.y, vec1.z * vec2.x - vec1.x * vec2.z, vec1.x * vec2.y - vec1.y * vec2.x);
}

float Vector3f::getMagnitude() {
	return sqrt(pow(y, 2) + pow(x, 2) + pow(z, 2));
}

Vector3f Vector3f::getNormalised() {
	float mag = getMagnitude();
	return Vector3f(x / mag, y / mag, z / mag);
}


