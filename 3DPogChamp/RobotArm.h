#pragma once
Material LowerArmMat = {
	{ 0.4, 0.0, 0.0, 1.0 }, // Ambient
	{ 0.9, 0.0, 0.0, 1.0 }, // Diffuse
	{ 0.8, 0.8, 0.8, 1.0 }, // Specular
	32 // Shininess
};
Material UpperArmMat = {
	{ 0.0, 0.4, 0.0, 1.0 }, // Ambient
	{ 0.0, 0.9, 0.0, 1.0 }, // Diffuse
	{ 0.8, 0.8, 0.8, 1.0 }, // Specular
	32 // Shininess
};


enum DrawFunction { Base, LowerArm, UpperArm, Joint };

struct TreeNode {
	GLfloat matrix[16];
	DrawFunction drawId;
	TreeNode* child;
	TreeNode* sibling;
};

class RobotArm {
public:
	RobotArm();
	~RobotArm();

	Vector3f position;
	float yRotBase, rotLowerArm, rotUpperArm;

	//models
	GLUquadric* baseNodeModel;
	GLUquadric* lowerArm;
	GLUquadric* upperArm;
	GLUquadric* joint;

	//texture
	GLubyte texture[64][64][3];

	TreeNode *baseNode;
	TreeNode *upperArmNode;
	TreeNode *lowerArmNode;
	TreeNode *lowerJointNode;
	TreeNode *upperJointNode;

public:
	void Start();
	void BuildTree();
	void RenderTree(TreeNode* root);
	void RenderFunction(DrawFunction id);
	void Render();
	void RenderBase();
	void RenderUpperArm();
	void RenderLowerArm();
	void RenderJoint();
	//key down
	void HandleKeyDown(WPARAM wParam);
	//make texture
	void CreateTexture();
	void SetMaterial(Material* mat);
};

