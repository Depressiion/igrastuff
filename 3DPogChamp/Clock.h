#pragma once
class Clock{
public:
	Clock();
	virtual ~Clock();

	__int64 startTimeCounts = 0;
	__int64 endTimeCounts = 0;
	double countsPerSecond = 0.0f;

	void StartTimer();
	__int64 GetCurrentCounts();
	float GetTimeSinceLastTime();
	float GetTotalTime();
};

