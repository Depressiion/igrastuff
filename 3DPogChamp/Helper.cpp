#include "stdafx.h"

Helper::Helper() {
}

Helper::~Helper() {
}

void Helper::Draw3DAxis() {
	glBegin(GL_LINES);
	{
		//size
		float length = 1000;
		// Draw positive x-axis as red
		glColor3f(1.0, 0.0, 0.0);
		glVertex3f(0, 0, 0);
		glVertex3f(length, 0, 0);
		// Draw positive x-axis as green
		glColor3f(0.0, 1.0, 0.0);
		glVertex3f(0, 0, 0);
		glVertex3f(0, length, 0);
		// Draw positive z-axis as blue
		glColor3f(0.0, 0.0, 1.0);
		glVertex3f(0, 0, 0);
		glVertex3f(0, 0, length);
	}
	glEnd();
}

void Helper::DrawNormal(MyPolygon::Polygon poly) {
	//find centerpoint of polygon
	float totalX = 0, totalY = 0, totalZ = 0;
	int totalCorners = poly.corners.size();

	//std::thread t1([&] {
	//	for (int count = 0; count < totalCorners / 2; count++) {
	//		totalX += poly.corners[count]->x;
	//		totalY += poly.corners[count]->y;
	//		totalZ += poly.corners[count]->z;
	//	}
	//});

	//std::thread t2([&] {
	//	for (int count = totalCorners / 2; count < totalCorners; count++) {
	//		totalX += poly.corners[count]->x;
	//		totalY += poly.corners[count]->y;
	//		totalZ += poly.corners[count]->z;
	//	}
	//});

	//t1.join();
	//t2.join();

	for (int count = 0; count < totalCorners; count++) {
		totalX += poly.corners[count]->x;
		totalY += poly.corners[count]->y;
		totalZ += poly.corners[count]->z;
	}

	Vector3f centerPoint(totalX / totalCorners, totalY / totalCorners, totalZ / totalCorners);	//start point
	Vector3f endPoint = centerPoint + poly.normal;

	glColor3f(0, 0, 0);
	glBegin(GL_LINES);
	{
		glVertex3f(endPoint.x, endPoint.y, endPoint.z);
		glVertex3f(centerPoint.x, centerPoint.y, centerPoint.z);
	}
	glEnd();
}

Vector3f Helper::FindNormal(Vector3f v1, Vector3f v2, Vector3f v3) {
	Vector3f V = Vector3f(v2.x - v1.x, v2.y - v1.y, v2.z - v1.z);
	Vector3f U = Vector3f(v3.x - v1.x, v3.y - v1.y, v3.z - v1.z);
	Vector3f oniiChan = Vector3f(U.y * V.z - U.z * V.y, U.z * V.x - U.x * V.z, U.x * V.y - U.y * V.x).getNormalised();
	oniiChan *= 0.5f;
	return oniiChan;
}

