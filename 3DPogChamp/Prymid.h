#pragma once
#include "stdafx.h"
struct Triangle : public MyPolygon::Polygon {
	Triangle(Vector3f* bottomLeft, Vector3f* top, Vector3f* bottomRight) {
		corners.push_back(bottomLeft);
		corners.push_back(top);
		corners.push_back(bottomRight);
	}
};

class Prymid : public GameObject {
public:
	Prymid();
	Prymid(Vector3f position, float height) {
		this->position = position;
		r = 0.5;
		heightMultiplier = height;
		Start();
	}
	virtual ~Prymid();
	/*Triangle* triangles[6];*/
	std::vector<Triangle*> triangles;
	float r = 0.5f;
	GLubyte texture[64][64][3];

	float heightMultiplier = 5;
	int rotationAmout = 0;

	std::vector<Vector3f> rotations;
	std::vector<int> rotationAmounts;

	void Start();
	void Update();
	void Render();
	void RenderNoMat();
	void RenderWireframe();
	void CreateTexture();
	void RenderOneFace();

	void InitCorners(float radius);
	void InitTriangles(std::vector<Vector3f*> corners);
};
#define DegreeToRadians(x) x * (3.1415 / 180)

class CircleHourGlass : public GameObject {
public:
	CircleHourGlass(Vector3f centerPos, float radius) : radius(radius) {
		this->position = centerPos;
	}
	~CircleHourGlass() {};

	float radius = 3;
	std::vector<Prymid*> hgs;

	void Start() {
		float reso = 360 / 8;
		for (int count = 0; count < 8; count++) {
			float x = 0, z = 0;
			x = position.x + radius * cos(DegreeToRadians(count * reso));
			z = position.z + radius * sin(DegreeToRadians(count * reso));

			//bottom
			Prymid* toBePushed = new Prymid(Vector3f(x, position.y - 1, z), 2);
			toBePushed->rotationAmounts.push_back(count * -reso + 180);
			toBePushed->rotations.push_back(Vector3f(0, 1, 0));
			hgs.push_back(toBePushed);

			//top
			toBePushed = new Prymid(Vector3f(x, position.y + 1, z), 2);
			toBePushed->rotationAmounts.push_back(180);
			toBePushed->rotations.push_back(Vector3f(1, 0, 0));
			toBePushed->rotationAmounts.push_back(count * reso);
			toBePushed->rotations.push_back(Vector3f(0, 1, 0));

			hgs.push_back(toBePushed);
		}
	}
	void Update() {

	}
	void Render() {
		for (int count = 0; count < hgs.size(); count++) {
			hgs[count]->RenderNoMat();
		}
	}
};

