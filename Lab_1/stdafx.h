// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

#include "targetver.h"

#define WIN32_LEAN_AND_MEAN             // Exclude rarely-used stuff from Windows headers
// Windows Header Files
#include <windows.h>

// C RunTime Header Files
#include <stdlib.h>
#include <malloc.h>
#include <memory.h>
#include <tchar.h>


// reference additional headers your program requires here
#include <iostream>
#include <gl\GL.h>
#include <gl\GLU.h>
#include "Helper.h"
#include "Vector2P.h"
#include <vector>
#include "Square.h"
#include <stdio.h>	//sprintf
#include <thread>	

// macros
#define print(x)std::cout << x << std::endl
#define PI 3.1415926535
#define WINDOW Vector2v(400, 400)
