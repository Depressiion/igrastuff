#pragma once
#include <string>
#include <gl/GL.h>
#include <gl/GLU.h>
namespace Vector2Personal {
	class Vector2v {
	public:
		Vector2v();
		Vector2v(float, float);
		virtual ~Vector2v();

		float x, y;

	public:
		float getMagnitude();
		Vector2v normalised();
		void normalise();
		std::string toString();
		bool equals(Vector2v);
		bool equals(Vector2v, float);

	public:
		//-
		Vector2v operator- (Vector2v);
		void operator -= (Vector2v);

		//+
		Vector2v operator+ (Vector2v);
		void operator+= (Vector2v);

		// /
		Vector2v operator/ (float);
		void operator/= (float);
		Vector2v operator/ (Vector2v);
		void operator/= (Vector2v);

		//*
		void operator*= (float);

	public:
		inline static Vector2v up() {	return Vector2v(0, 1);	}

		inline static Vector2v right() {	return Vector2v(1, 0); }

		inline static Vector2v zero() { return Vector2v(0, 0); }

	};

	Vector2v operator* (float, Vector2v);
}


