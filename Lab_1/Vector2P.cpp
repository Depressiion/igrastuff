#include "stdafx.h"
#include "Vector2P.h"
#include <math.h>
using namespace Vector2Personal;

Vector2v::Vector2v() {
}

Vector2v::Vector2v::Vector2v(float x, float y) : x(x), y(y) {

}


Vector2v::~Vector2v() {

}

float Vector2Personal::Vector2v::getMagnitude() {
	return sqrt(pow(x, 2) + pow(y, 2));
}

Vector2v Vector2Personal::Vector2v::normalised() {
	float mag = getMagnitude();
	return Vector2v(x / mag, y / mag);
}

void Vector2Personal::Vector2v::normalise() {
	float mag = getMagnitude();
	x /= mag;
	y /= mag;
}

std::string Vector2Personal::Vector2v::toString() {
	return std::to_string(x) + ", " + std::to_string(y);
}

Vector2v Vector2Personal::Vector2v::operator-(Vector2v input) {
	return Vector2v(x - input.x, y - input.y);
}

void Vector2Personal::Vector2v::operator-=(Vector2v input) {
	x -= input.x;
	y -= input.y;
}

Vector2v Vector2Personal::Vector2v::operator+(Vector2v input) {
	return Vector2v(x + input.x, y + input.y);
}

void Vector2Personal::Vector2v::operator+=(Vector2v input) {
	x += input.x;
	y += input.y;
}

Vector2v Vector2Personal::Vector2v::operator/(float input) {
	return Vector2v(x / input, y / input);
}

void Vector2Personal::Vector2v::operator/=(float input) {
	x /= input;
	y /= input;
}

Vector2v Vector2Personal::Vector2v::operator/(Vector2v input) {
	return Vector2v(x / input.x, y / input.y);
}

void Vector2Personal::Vector2v::operator/=(Vector2v input) {
	x /= input.x;
	y /= input.y;
}

void Vector2Personal::Vector2v::operator*=(float input) {
	x *= input;
	y *= input;
}

bool Vector2Personal::Vector2v::equals(Vector2v input) {
	return input.x == x && input.y == y;
}

bool Vector2Personal::Vector2v::equals(Vector2v input, float range) {
	return (input - *this).getMagnitude() < range;
}

Vector2v Vector2Personal::operator*(float timesBy, Vector2v input) {
	return Vector2v(timesBy * input.x, timesBy * input.y);
}
