#pragma once
#include "stdafx.h"
using namespace Vector2Personal;

int InitOpenGL();
void DrawGLScene();
void ReSizeGLScene(GLsizei, GLsizei);
void DrawSpiral(int, float);
void DrawTriangularSpiral(int);
void CircularSpiral(float);
void JumpingCircle();

Vector2v mousePos(0, 0);
Square sq(Vector2v(0.3f, 0.3f));

static PIXELFORMATDESCRIPTOR pfd{
 sizeof(PIXELFORMATDESCRIPTOR),
 1,// Version Number
 PFD_DRAW_TO_WINDOW | // Format Must Support Window
 PFD_SUPPORT_OPENGL | // Format Must Support OpenGL
 PFD_DOUBLEBUFFER,// Must Support Double Buffering
 PFD_TYPE_RGBA, // Request An RGBA Format
 16, // Select Our Color Depth = 16
 0, 0, 0, 0, 0, 0, // Color Bits Ignored
 0,// No Alpha Buffer
 0,// Shift Bit Ignored
 0,// No Accumulation Buffer
 0, 0, 0, 0, // Accumulation Bits Ignored
 32, // 32 Bit Z-Buffer (Depth Buffer)
 0,// No Stencil Buffer
 0,// No Auxiliary Buffer
 PFD_MAIN_PLANE,// Main Drawing Layer
 0,// Reserved
 0, 0, 0// Layer Masks Ignored
};

#include "resource.h"
