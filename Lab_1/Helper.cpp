#include "stdafx.h"
using namespace Vector2Personal;

Helper::Helper() {
}


Helper::~Helper() {
}

//default one
void Helper::DrawCircle(float radius, float cx, float cy, int noOfPoints, GLenum state, float r, float g, float b) {
	float step = 360 / noOfPoints;
	glColor3f(r, g, b);	//color
	glBegin(state);
	{
		float x = 0, y = 0;
		for (int count = 0; count <= noOfPoints - 1; count++) {
			x = cx + radius * cos(DegreeToRadians(step * count));
			y = cy + radius * sin(DegreeToRadians(step * count));
			glVertex2f(x, y);
		}
	}
	glEnd();
}

void Helper::DrawCircle(float radius, float x, float y, int noOfPoints) {
	Helper::DrawCircle(radius, x, y, noOfPoints, GL_LINE_LOOP, 0, 0, 0);
}

void Helper::DrawCircle(float radius, Vector2v center, int noOfPoints) {
	Helper::DrawCircle(radius, center.x, center.y, noOfPoints, GL_LINE_LOOP, 0, 0, 0);
}

void Helper::DrawSineCurve(Vector2v start, int numOfCurve, float scale, HDC& hDc) {
	glColor3f(0, 0, 0);
	glBegin(GL_LINE_STRIP);
	{
		for (float x = 0; x <= DegreeToRadians(360); x += 0.01f) {
			glVertex2f(x + start.x, scale * sin(x * DegreeToRadians(180 * numOfCurve)) + start.y);
		} 
	}
	glEnd();
}

void Helper::DrawDampSineCurve(Vector2v start, int numOfCurve, float scale) {
	glColor3f(0, 0, 0);
	glBegin(GL_LINE_STRIP);
	{
		for (float x = 0; x <= DegreeToRadians(360); x += 0.01f) {
			float y = scale * sin(x * DegreeToRadians(180 * numOfCurve));
			if (y < 0) {
				scale *= -0.7f;
			}
			glVertex2f(x + start.x, y);
		}
	}
	glEnd();
}

float Helper::DegreeToRadians(float degree) {
	return degree * PI / 180;
}

Vector2v Helper::WindowToGL(Vector2v windowCords, Vector2v winSize) {
	//normalise to winSize
	windowCords /= winSize;
	//multiply x by 2 and y by -2
	//higher mouseX, higher glX
	//higher mouseY, lower glX
	windowCords.x *= 2;
	windowCords.y *= -2;
	//fix the cordinates
	windowCords += Vector2v(-1, 1);
	return windowCords;
}
