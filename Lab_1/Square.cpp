#include "stdafx.h"
using namespace Vector2Personal;

Square::Square(Vector2v size) : size(size), isSelected(false), isEditing(false), glPos(0, 0) {
	topLeft = glPos + Vector2v(-size.x / 2, size.y / 2);
	topRight = glPos + Vector2v(size.x / 2, size.y / 2);
	bottomLeft = glPos + Vector2v(-size.x / 2, -size.y / 2);	
	bottomRight = glPos + Vector2v(size.x / 2, -size.y / 2);
}

Square::~Square() {
}

void Square::update(Vector2v mousePos) {
	//handleInput(mousePos);
	//render();	

	std::thread inputThread(&Square::handleInput, this, std::ref(mousePos));
	render();
	//join with main thread
	inputThread.join();
}

void Square::handleInput(Vector2v mousePos) {
	mousePos = Helper::WindowToGL(mousePos, WINDOW);
	if (isSelected) {
		glPos = mousePos - offset;
	} else if (isEditing) {
		float mag = (mousePos - glPos).getMagnitude() * 1.75f;
		size.x = mag;
		size.y = mag;
	}

	//set positon
	topLeft = glPos + Vector2v(-size.x / 2, size.y / 2);
	topRight = glPos + Vector2v(size.x / 2, size.y / 2);
	bottomLeft = glPos + Vector2v(-size.x / 2, -size.y / 2);
	bottomRight = glPos + Vector2v(size.x / 2, -size.y / 2);
}

void Square::render() {
	glColor3f(0, 0, isSelected ? 1 : 0);
	glBegin(GL_LINE_LOOP);
	{
		glVertex2f(topLeft.x, topLeft.y);
		glVertex2f(topRight.x, topRight.y);
		glVertex2f(bottomRight.x, bottomRight.y);
		glVertex2f(bottomLeft.x, bottomLeft.y);
	}
	glEnd();
}

void Square::buttonDown(Vector2v mousePos) {
	//aabb
	mousePos = Helper::WindowToGL(mousePos, WINDOW);
	offset = mousePos - glPos;
	float range = 0.07f;
	isEditing = mousePos.equals(topLeft, range) || mousePos.equals(topRight, range) || mousePos.equals(bottomRight, range) || mousePos.equals(bottomLeft, range);
	if (isEditing) return;
	isSelected = (mousePos.x > topLeft.x && mousePos.x < topRight.x && mousePos.y > bottomLeft.y && mousePos.y < topLeft.y);
}

void Square::buttonUp() {
	isSelected = false;
	isEditing = false;
}
