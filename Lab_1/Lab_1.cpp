// Lab_1.cpp : Defines the entry point for the application.
//

#include "stdafx.h"
#include "Lab_1.h"

#pragma region stuff added
int width = 400, height = 400;	//height and width of window

GLuint PixelFormat;
HDC hDC = NULL;
HWND hWnd = NULL;
HGLRC hRC = NULL;

enum Modes {
	Rainbow, Wireframe, Hourglass, Spiral, SpiralTriangle, SpiralSpiral, HelperClass,
	JumpCircle, SineCurve, SelectSquare
};
Modes mode = Modes::SelectSquare;
#pragma endregion

#define MAX_LOADSTRING 100
#define EXTRA_WIDTH 17
#define EXTRA_HEIGHT 50

// Global Variables:
HINSTANCE hInst;                                // current instance
WCHAR szTitle[MAX_LOADSTRING];                  // The title bar text
WCHAR szWindowClass[MAX_LOADSTRING];            // the main window class name

// Forward declarations of functions included in this code module:
ATOM                MyRegisterClass(HINSTANCE hInstance);
BOOL                InitInstance(HINSTANCE, int);
LRESULT CALLBACK    WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK    About(HWND, UINT, WPARAM, LPARAM);

int APIENTRY wWinMain(_In_ HINSTANCE hInstance,
	_In_opt_ HINSTANCE hPrevInstance,
	_In_ LPWSTR    lpCmdLine,
	_In_ int       nCmdShow) {
	UNREFERENCED_PARAMETER(hPrevInstance);
	UNREFERENCED_PARAMETER(lpCmdLine);

	// TODO: Place code here.
	AllocConsole();	//open console
	freopen("conin$", "r", stdin);	//read console input
	freopen("conout$", "w", stdout);	//write to file
	freopen("conout$", "w", stderr);
	printf("Debugging Window:\n");
	//console

	// Initialize global strings
	LoadStringW(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
	LoadStringW(hInstance, IDC_LAB1, szWindowClass, MAX_LOADSTRING);
	MyRegisterClass(hInstance);

	// Perform application initialization:
	if (!InitInstance(hInstance, nCmdShow)) {
		return FALSE;
	}

	HACCEL hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_LAB1));

	MSG msg;

	// Main message loop:
	while (GetMessage(&msg, nullptr, 0, 0)) {
		if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg)) {
			TranslateMessage(&msg);
			DispatchMessage(&msg);
			//igra
			DrawGLScene();
			SwapBuffers(hDC);
		}
	}

	return (int)msg.wParam;
}

//
//  FUNCTION: MyRegisterClass()
//
//  PURPOSE: Registers the window class.
//
ATOM MyRegisterClass(HINSTANCE hInstance) {
	WNDCLASSEXW wcex;

	wcex.cbSize = sizeof(WNDCLASSEX);

	wcex.style = CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc = WndProc;
	wcex.cbClsExtra = 0;
	wcex.cbWndExtra = 0;
	wcex.hInstance = hInstance;
	wcex.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_LAB1));
	wcex.hCursor = LoadCursor(nullptr, IDC_ARROW);
	wcex.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
	wcex.lpszMenuName = MAKEINTRESOURCEW(IDC_LAB1);
	wcex.lpszClassName = szWindowClass;
	wcex.hIconSm = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

	return RegisterClassExW(&wcex);
}

//
//   FUNCTION: InitInstance(HINSTANCE, int)
//
//   PURPOSE: Saves instance handle and creates main window
//
//   COMMENTS:
//
//        In this function, we save the instance handle in a global variable and
//        create and display the main program window.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow) {
	hInst = hInstance; // Store instance handle in our global variable

	//HWND hWnd = CreateWindowW(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW,
	//	CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, nullptr, nullptr, hInstance, nullptr);

	hWnd = CreateWindowW(szWindowClass,
		szTitle, WS_OVERLAPPEDWINDOW,
		0, 0, width + EXTRA_WIDTH, height + EXTRA_HEIGHT,
		nullptr, nullptr, hInstance, nullptr);	//igra code

	InitOpenGL();//igra

	if (!hWnd) {
		return FALSE;
	}

	ShowWindow(hWnd, nCmdShow);
	UpdateWindow(hWnd);
	ReSizeGLScene(width, height);

	return TRUE;
}

//
//  FUNCTION: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  PURPOSE: Processes messages for the main window.
//
//  WM_COMMAND  - process the application menu
//  WM_PAINT    - Paint the main window
//  WM_DESTROY  - post a quit message and return
//
//
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam) {
	mousePos = Vector2v(LOWORD(lParam), HIWORD(lParam));

	switch (message) {
	case WM_COMMAND:
	{
		int wmId = LOWORD(wParam);
		// Parse the menu selections:
		switch (wmId) {
		case IDM_ABOUT:
			DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);
			break;
		case IDM_EXIT:
			DestroyWindow(hWnd);
			break;
		default:
			return DefWindowProc(hWnd, message, wParam, lParam);
		}
	}
	break;
	case WM_PAINT:
	{
		PAINTSTRUCT ps;
		HDC hdc = BeginPaint(hWnd, &ps);
		// TODO: Add any drawing code that uses hdc here...
		EndPaint(hWnd, &ps);
		break;
	}

	case WM_SIZE:
	{
		ReSizeGLScene(LOWORD(lParam), HIWORD(lParam));
		break;
	}

	case WM_KEYUP:
	{
		break;
	}

	case WM_KEYDOWN:
	{
		break;
	}

	case WM_LBUTTONDOWN:
	{
		sq.buttonDown(mousePos);
		break;
	}

	case WM_LBUTTONUP:
	{
		sq.buttonUp();
		break;
	}

	case WM_DESTROY:
		PostQuitMessage(0);
		break;

	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
	}
	return 0;
}

// Message handler for about box.
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam) {
	UNREFERENCED_PARAMETER(lParam);
	switch (message) {
	case WM_INITDIALOG:
		return (INT_PTR)TRUE;

	case WM_COMMAND:
		if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL) {
			EndDialog(hDlg, LOWORD(wParam));
			return (INT_PTR)TRUE;
		}
		break;
	}
	return (INT_PTR)FALSE;
}

//igra stuff
int InitOpenGL() {
	// Get Device Dontext
	if (!(hDC = GetDC(hWnd))) {
		MessageBox(NULL, L"Can't Create A GL Device Context.",
			L"ERROR", MB_OK | MB_ICONEXCLAMATION);
		return 0;
	}
	// Check if Windows can find a matching Pixel Format
	if (!(PixelFormat = ChoosePixelFormat(hDC, &pfd))) {
		MessageBox(NULL, L"Can't Find A Suitable PixelFormat.",
			L"ERROR", MB_OK | MB_ICONEXCLAMATION);
		return 0;
	}
	// Try to set pixel format
	if (!SetPixelFormat(hDC, PixelFormat, &pfd)) {
		MessageBox(NULL, L"Can't Set The PixelFormat.", L"ERROR",
			MB_OK | MB_ICONEXCLAMATION);
		return 0;
	}
	// Get a Rendering context
	if (!(hRC = wglCreateContext(hDC))) {
		MessageBox(NULL, L"Can't Create A GL Rendering Context.",
			L"ERROR", MB_OK | MB_ICONEXCLAMATION);
		return 0;
	} // Activate Rendering Context
	if (!wglMakeCurrent(hDC, hRC)) {
		MessageBox(NULL, L"Can't Activate The GL Rendering Context.", L"ERROR", MB_OK | MB_ICONEXCLAMATION);
		return 0;
	}

	// TO SOLVE DEPTH-BUFFER ISSUE
	glClearDepth(1.0f);

	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
	return 1;


}

void DrawGLScene() {
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glClearColor(1, 1, 1, 1); // White

	glBegin(GL_LINES);
		glColor3f(0, 1, 0);	//g
		glVertex2f(0, 1);
		glVertex2f(0, 0);
		glColor3f(1, 0, 0);//r
		glVertex2f(0, 0);
		glVertex2f(1, 0);
	glEnd();

	switch (mode) {
	case Modes::Rainbow:
		// Draw a colourful triangle
		glBegin(GL_POLYGON);
		glColor3f(1, 0, 0); // Red
		glVertex2f(0, 0.5); // Top
		glColor3f(0, 1, 0); // Green
		glVertex2f(-0.5, -0.5); // Left
		glColor3f(0, 0, 1); // Blue
		glVertex2f(0.5, -0.5); // Right
		glEnd();
		break;

	case Modes::Wireframe:
		glBegin(GL_LINE_LOOP);
		glColor3f(0, 0, 0);
		glVertex2f(0, 0.5); // Top
		glVertex2f(-0.5, -0.5); // Left
		glVertex2f(0.5, -0.5); // Right
		glEnd();
		break;

	case Modes::Hourglass:
		glBegin(GL_LINE_LOOP);
		glColor3f(0, 0, 0);
		glVertex2f(-0.5f, 0.5f);
		glVertex2f(0.5f, 0.5f);
		glVertex2f(-0.5f, -0.5f);
		glVertex2f(0.5f, -0.5f);
		glEnd();
		break;

	case Modes::Spiral:
		DrawSpiral(50, 0.05f);
		break;

	case Modes::SpiralTriangle:
		DrawTriangularSpiral(3);
		break;

	case Modes::SpiralSpiral:
		CircularSpiral(0.01f);
		break;

	case Modes::HelperClass:
		Helper::DrawCircle(0.2f, -0.3, 0, 36, GL_LINE_LOOP, 1, 0, 0);
		Helper::DrawCircle(0.2f, 0.3f, 0, 36, GL_POLYGON, 1, 0, 0);

		Helper::DrawCircle(0.2f, 0, -0.3f, 36, GL_LINE_LOOP, 0, 0, 1);
		Helper::DrawCircle(0.2f, 0, 0.3f, 36, GL_POLYGON, 0, 0, 1);
		break;

	case Modes::JumpCircle:
		JumpingCircle();
		break;

	case Modes::SineCurve:
		Helper::DrawDampSineCurve(Vector2v(-1, 0), 3, 0.5f);

		//Helper::DrawSineCurve(Vector2v(-1, 0), 3, 0.5f, hDC);
		break;

	case Modes::SelectSquare:
		sq.update(mousePos);
		break;

	default:
		break;
	}
}

void JumpingCircle() {
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glClearColor(1, 1, 1, 1); // White

	glBegin(GL_LINES);
	glColor3f(0, 1, 0);	//g
	glVertex2f(0, 1);
	glVertex2f(0, 0);
	glColor3f(1, 0, 0);//r
	glVertex2f(0, 0);
	glVertex2f(1, 0);
	glEnd();
	//draw circle
	Helper::DrawCircle(0.5f, Helper::WindowToGL(mousePos, Vector2v(width, height)), 20);
}

void ReSizeGLScene(GLsizei width, GLsizei height) {
	if (height == 0)// Prevent A Divide By Zero By
		height = 1; // Making Height Equal One
		// Reset The Current Viewport
	glViewport(0, 0, width, height);
	// Select The Projection Matrix
	glMatrixMode(GL_PROJECTION);
	// Reset The Projection Matrix
	glLoadIdentity();
	// Calculate The Aspect Ratio Of The Window
	//gluPerspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);
	gluOrtho2D(-1.0, 1.0, -1.0, 1.0);

}

void DrawSpiral(int noOfLines, float bumpValue) {
	float rn = bumpValue;
	float x = 0, y = 0;
	glBegin(GL_LINE_STRIP);
	glColor3f(0, 0, 0);
	glVertex2f(x, y);
	for (int count = 0; count <= noOfLines; count++) {
		if (count % 2 == 0) {
			x += rn;
		} else {
			y += rn;
			rn += bumpValue * (rn < 0 ? -1 : 1);
			rn *= -1;
		}
		glVertex2f(x, y);
	}
	glEnd();
}

void DrawTriangularSpiral(int noOfCorners) {
	glBegin(GL_LINE_STRIP);
	float dRot = 360 / noOfCorners;
	float radius = 0.1f;
	Vector2v currentPos(0, 0);
	int degree = 0;
	int noOfLines = 10;
	float dRotTotal = 0;

	glColor3f(0, 0, 0);
	for (int count = 0; count <= noOfLines - 1; count++) {
		dRotTotal += dRot;
		degree += dRot;
		degree %= 360;

		if (dRotTotal > 90) {
			radius += 0.1f;
			dRotTotal = 0;
		}

		float dx = radius * cos(Helper::DegreeToRadians(degree));
		float dy = radius * sin(Helper::DegreeToRadians(degree));
		currentPos += Vector2v(dx, dy);
		glVertex2f(currentPos.x, currentPos.y);
	}
	glEnd();
}

void CircularSpiral(float radius) {
	glBegin(GL_LINE_STRIP);
	glColor3f(0, 0, 0);
	float x = 0, y = 0;

	for (float degree = 0; degree <= 360; degree += 0.01f) {
		radius = (sin(3 * degree)) * 0.5f;
		x = radius * cos(degree);
		y = radius * sin(degree);
		glVertex2f(x, y);
	}

	glEnd();
}




