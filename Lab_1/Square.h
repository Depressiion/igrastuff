#pragma once
class Square {
public:
	Vector2v size;
	Vector2v glPos;
	Vector2v offset;

	Vector2v topLeft;
	Vector2v topRight;
	Vector2v bottomLeft;
	Vector2v bottomRight;

	bool isSelected = false, isEditing = false;

	Square(Vector2v size);
	~Square();

	void update(Vector2v);
	void render();
	void handleInput(Vector2v);
	void buttonDown(Vector2v);
	void buttonUp();
};

