#pragma once
#include "Vector2P.h"
using namespace Vector2Personal;
class Helper {
public:
	Helper();
	virtual ~Helper();

	//circle
	//default
	static void DrawCircle(float radius, float x, float y, int noOfPoints, GLenum state, float r, float g, float b);
	static void DrawCircle(float radius, float x, float y, int noOfPoints);
	static void DrawCircle(float radius, Vector2v center, int noOfPoints);

	//sine
	static void DrawSineCurve(Vector2v start, int numOfCurve, float scale, HDC& hDc);
	static void DrawDampSineCurve(Vector2v start, int numOfCurve, float scale);

	//helpers
	static float DegreeToRadians(float degree);
	static Vector2v WindowToGL(Vector2v windowCords, Vector2v windSize);


};

